# Socialite
**Socialite** is a basic version of Pinterest that allows users to follow each other, bookmark images from the web and store them so that they can be shared with the followers along with having the ability to react to them. Users can login with username and password or can login through their Google or Facebook or Twitter account. This is a Django based web application.
